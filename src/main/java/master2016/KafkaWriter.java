package master2016;

import java.util.List;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.PartitionInfo;

public class KafkaWriter {

	private KafkaProducer<String, String> prod;
	private Set<String> langSet;
	
	public KafkaWriter(String brokerUrl){
		// Producer properties
		Properties props = new Properties();
	    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerUrl);
	    props.put("acks", "all");
	    props.put("retries", 0);
	    props.put("batch.size", 16384);
	    props.put("buffer.memory", 33554432);
	    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
	    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
	    // Create the producer
	    prod = new KafkaProducer<String, String>(props);
	    
	    
	    // For the consumer
	    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	    // Get all existing topics 
	    KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
	    Map<String, List<PartitionInfo>> topics = consumer.listTopics();
	    consumer.close();
	    
	    String langs = "";
	    for(String s: topics.keySet()){
	        // Lang codes are two chars
	        if(s.length()==2)
	            langs += s + ",";
	    }
	    langs = langs.substring(0, langs.length()-1); // Removing the final comma
	    System.out.println("********* WRITER: language set********");
	    System.out.println(langs);
	    System.out.println("**************************************");
	    langSet = new HashSet<String>(Arrays.asList(langs.split(",")));
	}

	public void send(String lang, String findHashtag){
		if(findHashtag.length()>0){
            findHashtag=findHashtag.substring(0, findHashtag.length()-1);
			// Filter by language
	        if(langSet.contains(lang)){
	            System.out.println("Writing " + lang + " Hashtags:" + findHashtag);
	            prod.send(new ProducerRecord<String, String>(lang,0,lang, findHashtag));
	        }
		}
	}
	

}
