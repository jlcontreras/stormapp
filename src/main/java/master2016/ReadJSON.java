package master2016;

import java.io.FileReader;
import java.util.Iterator;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class ReadJSON {
	private  String path;
	private KafkaWriter writer;
	
	public static void main(String[] args){
		KafkaWriter writer = null;
		ReadJSON r = new ReadJSON("/Users/Cadarso/Desktop/Cognitive systems/tweets_macbook_11-2.txt",writer);
		r.readTweets();
	}

	public ReadJSON(String path,KafkaWriter writer){
		this.path=path;
		this.writer = writer;
	}

   public  void readTweets() {
        JSONParser parser = new JSONParser();
 
        try {
 
            Object obj = parser.parse(new FileReader(path));
            //JSONObject jsonObject = (JSONObject) obj;
            JSONArray json = (JSONArray)obj;
            for (Object object : json) {
                JSONObject aJson = (JSONObject) object;
                JSONObject hashtag1 = ( JSONObject) aJson.get("entities");
                JSONArray hashtag = ( JSONArray) hashtag1.get("hashtags");
                String lang = (String) aJson.get("lang");
                //System.out.println(lang);
                String findHashtag ="";
                for(int i = 0; i < hashtag.size(); i++)
                {
                      JSONObject objects = (JSONObject) hashtag.get(i);
                      String name = (String)  objects.get("text");
                      //System.out.println(name);
                      findHashtag= findHashtag.concat(name+",");
                      //Iterate through the elements of the array i.
                      //Get thier value.
                      //Get the value for the first element and the value for the last element.
                }
                writer.send(lang, findHashtag);
            }
           
 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
