package master2016;


import java.io.IOException;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * @author Yusuke Yamamoto - yusuke at mac.com
 * @since Twitter4J 2.1.7
 */
public class TwitterStreaming {
	
    private String apiKey, apiSecret, tokenValue, tokenSecret;
    private KafkaWriter writer;

    /**
     * Constructor
     */
    public TwitterStreaming(String apiKey, String apiSecret, String tokenValue, String tokenSecret, KafkaWriter writer){
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
        this.tokenValue = tokenValue;
        this.tokenSecret = tokenSecret;
        this.writer = writer;
    }  
	
    /**
     * Usage: java twitter4j.examples.oauth.GetAccessToken [consumer key]
     * [consumer secret]
     *
     * @param args
     *            message
     */
	public static void main(String[] args) throws TwitterException, IOException {

		TwitterStreaming ts = new TwitterStreaming("T90xz75HnOwL3igNryDUaZtfw", 
                                    "4nMFuhqmvRBkuz45hWBdgYvJDtsC8dby3YPuEuZgiB0FAtPSZI",
                                    "801070148354527233-hZXV3nG4xHD0b1wLNIW29O0kBAThHyk",
                                    "DFUPXmIeTAJVVC8cKenIiCp4lZ4kxZ0l9cbv6FyLocjOC",
                                    new KafkaWriter("localhost:9092"));
		ts.readTweets();

	}

    public void readTweets(){
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey(apiKey)
                .setOAuthConsumerSecret(apiSecret)
                .setOAuthAccessToken(tokenValue)
                .setOAuthAccessTokenSecret(tokenSecret);
        

        StatusListener listener = new StatusListener() {         
            public void onStatus(Status status) {
                String findHashtag = "";
                if (status.getHashtagEntities().length > 0) {
                    for (int i = 0; i < status.getHashtagEntities().length; i++) {
                        findHashtag = findHashtag.concat(status.getHashtagEntities()[i].getText() + ",");
                    }
                    String lang = status.getLang();
                    writer.send(lang, findHashtag);
                }

                // System.out.println(status.getUser().getName() + " : " +
                // status.getText());
            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            }

            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            }

            public void onException(Exception ex) {
                ex.printStackTrace();
            }

            public void onScrubGeo(long arg0, long arg1) {
                // TODO Auto-generated method stub

            }

            public void onStallWarning(StallWarning arg0) {
                // TODO Auto-generated method stub

            }
        };
        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        
        twitterStream.addListener(listener);
        twitterStream.sample();
    }
}
