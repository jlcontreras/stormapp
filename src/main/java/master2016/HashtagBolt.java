package master2016;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

public class HashtagBolt extends BaseRichBolt{

	private final String groupID = "07";
	private static final long serialVersionUID = -3924515801464062647L;
	private String hashtagsField, language, token, outputFile; // Names of both input fields
	private TreeMap<String, Integer> htCount; // TreeMap is sorted based on keys
	private boolean counting;
	private int windowCount;

	/**
	 * Constructor
	 * @param groupID: our group id (used to get the output file name)
	 */
	public HashtagBolt(String language, String token, String hashtagsField, String outputPath){
		this.hashtagsField = hashtagsField;
		this.language = language;
		this.token = token;
		outputFile = outputPath + language + "_" + groupID + ".log";
		
		counting = false;
		windowCount = 0;
		htCount = new TreeMap<String, Integer>();
		
		System.out.println("TOKEN: " + token);
		
	}
	
	/**
	 * Called every time an input is received (each tuple contains a list of hashtags and the corresponding language)
	 * TODO: Probar que cada tuple tenga language y un solo hashtag
	 */
	public void execute(Tuple input) {
		// Get language and hashtag
		String hashtags = ((String)input.getValueByField(hashtagsField)).toLowerCase();
		// Iterate through received hashtags and increment the count for each of them
		for(String htag: hashtags.split(",")){
			//System.out.println("B:" + htag);
			if(counting){
				// If we are counting, and the received hashtag is not the token, count it 
				if(!token.equalsIgnoreCase(htag)){
					Integer num = htCount.get(htag);
					if(null != num){
						htCount.put(htag, ++num);
						System.out.println("COUNT: " + htag + " = " + num);
					} else {
						htCount.put(htag, 1);
						System.out.println("COUNT: " + htag + " = 1");
					}
					// Otherwise, close the window
				} else if(token.equalsIgnoreCase(htag)){	
					String topEntries = "";

					// No need to worry about alphabetical order because TreeMap is ordered alphabetically
					// Look for the top 3 hashtags and put them in the return string
					for(int i = 0; i<3; i++){
						Entry<String,Integer> maxEntry = null;
						for(Entry<String,Integer> entry : htCount.entrySet()) {
					    	if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
					        	maxEntry = entry;
					    	}
						}
						if(null == maxEntry){
							topEntries = topEntries + ",null,0";
						}else{
							topEntries = topEntries + "," + maxEntry.getKey() + "," + maxEntry.getValue();
							htCount.remove(maxEntry.getKey());
						}
					}
					windowCount++;
					
					String results = windowCount + "," + language + topEntries;
					
					System.out.println("********************");
					System.out.println("Writting to file : " + results);
					System.out.println("********************");
					
					writeResults(results);
					
					//--------------------------
					
					htCount.clear();
					counting = false;
					return;
				}
			} else if (!counting) {
				// If it's the window delimiter, start counting
				if(token.equalsIgnoreCase(htag)){
					counting = true;
					System.out.println("Counting----------------------------------------");
				}
			}
					
		}
	}
	
	/**
	 * Conveniency method for writting output to a file
	 */
	private void writeResults(String s){
		try
		{
			FileWriter fw = new FileWriter(outputFile, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
		    out.println(s);
		    out.close();
			fw.close();
		}
		catch (IOException e) 
		{
			// TODO: error
		}
	}
	
	// Non-serializable stuff should be created here
	public void prepare(Map arg0, TopologyContext arg1, OutputCollector collector) {
		//this.collector = collector;
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		//declarer.declareStream(AVGTEMP_STREAM, new Fields("roomID", AVGTEMP_FIELD));		
	}

}
