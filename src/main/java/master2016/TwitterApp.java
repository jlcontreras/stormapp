package master2016;

public class TwitterApp {
	
	/**
	 * Params (in order):
	 * mode: 1 means read from file, 2 read from the Twitter API.
	 * apiKey: key associated with the Twitter app consumer.
	 * apiSecret: secret associated with the Twitter app consumer.
	 * tokenValue: access token associated with the Twitter app.
	 * tokenSecret: access token secret.
	 * Kafka Broker URL: String in the format IP:port corresponding with the Kafka Broker
	 * Filename
	 */
	public static void main(String[] args){
		
		/*
		if(args.length != 7){	
		
		}*/
		
		/*
		// Get config parameters
		int mode = Integer.parseInt(args[0]);
		String apiKey = args[1]; "T90xz75HnOwL3igNryDUaZtfw"
		String apiSecret = args[2];
		String tokenValue = args[3];
		String tokenSecret = args[4];
		String brokerUrl = args[5];
		String fileName = args[6];
		*/

		int mode = 1;
		String apiKey = "T90xz75HnOwL3igNryDUaZtfw";
		String apiSecret = "4nMFuhqmvRBkuz45hWBdgYvJDtsC8dby3YPuEuZgiB0FAtPSZI";
		String tokenValue = "801070148354527233-hZXV3nG4xHD0b1wLNIW29O0kBAThHyk";
		String tokenSecret = "DFUPXmIeTAJVVC8cKenIiCp4lZ4kxZ0l9cbv6FyLocjOC";
		String brokerUrl = "localhost:9092";
		String fileName = "/home/jose/Documents/t.txt";

		KafkaWriter writer = new KafkaWriter(brokerUrl);
		
		// Mode 1: Read from file
		if(mode == 1)
		{
			System.out.println("Reading tweets from file: " + fileName);
			ReadJSON r = new ReadJSON(fileName, writer);
			r.readTweets();
		}
		
		// Mode 2: Read from Twitter's Streaming API
		else if(mode == 2)
		{
			System.out.println("Streaming tweets from Twitter");
			TwitterStreaming ts = new TwitterStreaming(apiKey, apiSecret, tokenValue, tokenSecret, writer);
			ts.readTweets();
		}
		
	}
	
}
