package master2016;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

/**
 * This spout reads data from a CSV file. It is only suitable for testing in local mode
 */
public class MyKafkaSpout extends BaseRichSpout {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3659308993456296464L;
	private String streamName, fieldName, lang, kafkaBroker;
	private SpoutOutputCollector collector;
	private KafkaConsumer<String, String> consumer;

	public MyKafkaSpout(String streamName, String fieldName, String lang, String kafkaBroker){
		this.streamName = streamName;
		this.fieldName = fieldName;
		this.lang = lang;
		this.kafkaBroker = kafkaBroker;
	}
	
	/**
	 * Prepare the spout. This method is called once when the topology is submitted
	 * @param conf
	 * @param context
	 * @param collector
	 */
	
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
		
		// Set up the Kafka consumer 
		this.collector=collector;
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,kafkaBroker);
		props.put("group.id", "MYGROUP");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		consumer = new KafkaConsumer<String, String>(props);
		consumer.subscribe(Arrays.asList(lang));
	}

	@Override
	public void deactivate() {
		
	}

	/**
	 * Storm will call this method repeatedly to pull tuples from the spout
	 */
	public void nextTuple() {
		ConsumerRecords<String, String> records = consumer.poll(10);
		for (ConsumerRecord<String, String> record : records){
			System.out.print("Topic: " + record.topic() + ", ");
			System.out.print("Partition: " + record.partition() + ", "); 
			System.out.print("Key:" + record.key() + ", ");
			System.out.println("Value: " + record.value() );

			Values val = new Values(record.value());
			collector.emit(streamName, val);

		}

	}

	/**
	 * Storm will call this method when tuples are acked
	 * @param id
	 */
	@Override
	public void ack(Object id) {
	}

	/**
	 * Storm will call this method when tuples fail to process downstream
	 * @param id
	 */
	@Override
	public void fail(Object id) {
		System.err.println("Failed line number " + id);
	}

	/**
	 * Tell storm which fields are emitted by the spout
	 * @param declarer
	 */
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// read csv header to get field info
		declarer.declareStream(streamName, new Fields(fieldName));
	}

}