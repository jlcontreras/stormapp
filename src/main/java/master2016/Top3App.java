package master2016;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.utils.Utils;

/**
 * Hello world!
 *
 */
public class Top3App 
{

    public static final String HASHTAGS_FIELD = "hashtag";
    public static final String TWITTER_STREAMNAME = "twitterStream";

    /**
     * Arguments:
     * langList: String with the list of languages (e.g: en:house,pl:universidade,ar:carro)
     * Kafka Broker URL: String IP:port of the Kafka Broker.
     * topologyName: String identifying the topology in the Storm Cluster.
     * folder: path to the folder used to store the output files
     */
    public static void main( String[] args ) throws Exception
    {   
        // Check that the number of args is correct
        /*if(args.length != 4){
            throw new Exception("Invalid number of arguments");
        }*/
    	System.out.println("***************Starting**************");
    	
        /*
        String[] langlist = args[0];
        String kafkaBroker = args[1];
        String topologyName = args[2];
        String folder = args[3];
    	*/

        String[] langlist = {"en:MacBook"};
        String kafkaBroker = "localhost:9092";
        String topologyName = "TrendingTopology";
        String folder = "/home/jose/Documents/";

        TopologyBuilder builder = new TopologyBuilder();
        
        
        for(String pair: langlist){
            String lang = pair.split(":")[0];
            String token = pair.split(":")[1];

            /*SpoutConfig kafkaConfig = new SpoutConfig(brokerHosts, TOPIC_NAME, "", "storm");
            builder.setSpout("twitterSpout"+lang, new KafkaSpout(kafkaConfig));*/

            builder.setSpout(lang + "twitterSpout", new MyKafkaSpout(TWITTER_STREAMNAME, HASHTAGS_FIELD, lang, kafkaBroker));    
            
            //builder.setSpout("twitterSpout", new LineSpout("test.txt", TWITTER_STREAMNAME, HASHTAGS_FIELD));    
        
            // As there is one bolt per spout we can use shufflegrouping
            builder.setBolt(lang + "hashtagBolt", new HashtagBolt(lang, token, HASHTAGS_FIELD, folder))
                    .shuffleGrouping(lang + "twitterSpout", TWITTER_STREAMNAME); // .setNumTasks(n) -- num executors defined as third param to shuffleGrouping
        }
        
        //builder.setBolt("filterBolt2", new FilterBolt("temp", 70, "A2: room %s, temp = %f"))
    	//	.shuffleGrouping("tempSpout", TempSpout.TEMPSTREAM_NAME); // Subscribe to stream tempStream from spout tempSpout
        // Good idea to distribute streams using fieldGrouping instead of shuffleGrouping
        // .fieldGrouping("tempSpout", "tempStream", new Fields("roomID")) // All tuples with the same roomID will be sent to the same Bolt
        
        Config conf = new Config();
        conf.put(Config.TOPOLOGY_DEBUG, false);

        LocalCluster lcluster = new LocalCluster();
        lcluster.submitTopology(topologyName, conf, builder.createTopology());
        
        // Run for 15 seconds before shutting down
        Utils.sleep(45000);
                
        lcluster.shutdown();
        
    }
}
